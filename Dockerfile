FROM liquibase/liquibase:4.3.2 AS liquibase
FROM postgres:13.2-alpine

EXPOSE 5432

COPY --from=liquibase /liquibase /liquibase
COPY entrypoint.sh /usr/local/bin
COPY run-liquibase.sh /usr/local/bin

WORKDIR /liquibase/changelog
COPY default.properties .

RUN  apk add openjdk11

ENV PATH $PATH:/liquibase
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres
ENV LIQUIBASE_DEFAULTS default.properties

ENTRYPOINT ["entrypoint.sh"]
