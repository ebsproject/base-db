# EBS BASE DATABASE

base-db is a lightweight image based on alpine linux and is intended to be the base image for other EBS Database images

base-db provides a unified environment with

- postgres 13.2
- liquibase 4.3.2

By using this image database containers can be quickly created and destroyed, reducing build and run times.

## Local Build

Do a basic build

    docker build -t base-db .

Although a numeric version is preferred for better control:

    docker build -t base-db:0.1 .


## Build for Dockerhub

More formal tags are recommended. E.g.:

    docker build -t ebsproject/base-db:0.1 -t ebsproject/base-db .


## Customize

### Override defaults

By default it tries to connect to local db configures with environment variables.
To override this behaviour provide a custom properties file:

1. Set the variable to specify the alternate configuration. Example: ```ENV LIQUIBASE_DEFAULTS default.properties```
2. copy  the configuration in liquibase folder. Example: ```COPY myCustom.properties .```

Make sure the values for postgres user,password and database match the ones in the environment.
Note that this variables are ultimately defined by the child containers if they decide not to run with de defaults.
